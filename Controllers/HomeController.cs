﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HuskyCompetitionWeb.Models;
using HuskyCompetitionWeb.ViewModels;
using System.IO;


namespace HuskyCompetitionWeb.Controllers
{
    public class HomeController : Controller
    {
        #region Display Members

        // Show the main list of mambers of the competition
        [HttpGet]
        public ActionResult Members(int PageNamber = 0,int SortOption = 0)
        {
            ServerDbRepository repository = new ServerDbRepository();

            // Creating Default list withou any sorting
            var List = repository.GetDisplayViewModelsList().ToList();

            //Calculating pagination for "Members" page.
            CalculatePagination(repository, PageNamber);
            ViewBag.SortId = SortOption;
            switch (SortOption)
            {
                case 1:

                    var ListOrderByCategiries = (from elements in List select elements).
                    OrderBy(elements => elements.CategoryData.Name).ToList();

                    return View(ListOrderByCategiries);
                
                case 2:

                    var ListOrderByName = (from elements in List select elements).
                        OrderBy(elements => elements.DogData.Name).ToList();
                    return View(ListOrderByName);                    
            }                      

            return View(repository.GetDisplayViewModelsList());
        }

        // Show detail information about each members from selected
        public ActionResult MembersDetail()
        {
            return RedirectToAction("Members", "Home");
        }
        [HttpPost]
        public ActionResult MembersDetail(int SelectedDetailID)
        {
            ServerDbRepository repository = new ServerDbRepository();
            foreach(DisplayViewModel model in repository.GetDisplayViewModelsList())
                if(model.MemberID == SelectedDetailID)
                    return View(model);
            return RedirectToAction("Members","Home");
        }

        #endregion


        // Display the main Page with descriptions about project and main actions
        public ActionResult Index()
        {
            ServerDbRepository repository = new ServerDbRepository();
            return View(repository.GetCategories().ToList());
        }

        #region Registration a new Member of Competition

        // The First step of registration new competition member.
        // Entering general information about Person which is the owner of a dog.
        public ActionResult InsertMemberStepOne()
        {
            InsertViewModel Model = new InsertViewModel();
            ViewBag.Message = "Here you can add a new member in the rating";
            ViewBag.StepNumber = 1;
            return View(Model);
        }
        [HttpPost]
        public ActionResult InsertMemberStepOne(InsertViewModel model)
        {
            if (ModelState.IsValid)
            {
                List<SelectListItem> items = new List<SelectListItem>();

                items.Add(new SelectListItem { Text = "Summer Season", Value = "Summer Season" });

                items.Add(new SelectListItem { Text = "Winter Season", Value = "Winter Season" });

                items.Add(new SelectListItem { Text = "Autumn Season", Value = "Autumn Season", Selected = true });

                items.Add(new SelectListItem { Text = "Spring Season", Value = "Spring Season" });

                ViewBag.CategorySelected = items;

                return View("InsertMemberStepTwo", model);
            }
            return View(model);
        }

        // The Second step - user entering additional information.
        // Dog name, age and more information about dog.
        public ActionResult InsertMemberStepTwo()
        {
            return RedirectToAction("InsertMemberStepOne");
        }
        [HttpPost]
        public ActionResult InsertMemberStepTwo(InsertViewModel model, string CategorySelected, HttpPostedFileBase UploadImage)
        {
            // Cleaning temp location before writing
            imageData = null;
            if (ModelState.IsValid)
            {
                if (UploadImage != null)
                {
                    
                    // Reading gived image file into byte mass
                    using (var binaryReader = new BinaryReader(UploadImage.InputStream))
                    {
                        imageData = binaryReader.ReadBytes(UploadImage.ContentLength);
                    }
                    // Delegated in model for preview
                    model.DogData.Image = imageData;
                }
                ViewBag.CategorySelected = CategorySelected;
                return View("InsertMemberStepThree", model);
            }
            List<SelectListItem> items = new List<SelectListItem>();

            items.Add(new SelectListItem { Text = "Summer Season", Value = "Summer Season" });

            items.Add(new SelectListItem { Text = "Winter Season", Value = "Winter Season" });

            items.Add(new SelectListItem { Text = "Autumn Season", Value = "Autumn Season", Selected = true });

            items.Add(new SelectListItem { Text = "Spring Season", Value = "Spring Season" });

            ViewBag.CategorySelected = items;
            return View("InsertMemberStepTwo", model);
        }

        // The Finish step - user  validate entered information.
        // And if ewerything is correct, submit information to the server.
        public ActionResult InsertMemberStepThree()
        {
            return RedirectToAction("InsertMemberStepOne");
        }
        [HttpPost]
        public ActionResult InsertMemberStepThree(InsertViewModel model, string CategorySelected)
        {
            // Using repository for working with Data Base.
            ServerDbRepository repository = new ServerDbRepository();

            repository.InsertOwner(new Owner
            {
                Name = model.OwnerData.Name,
                Surname = model.OwnerData.Surname,
                Email = model.OwnerData.Email,
                Info = model.OwnerData.Info
            });
            repository.Save();

            repository.InsertDog(new Dog
            {
                Name = model.DogData.Name,
                Age = model.DogData.Age,
                Image = imageData,
                OwnerID = repository.GetLastOwner().ID
            });
            repository.Save();

            repository.InsertMember(new Member
            {
                DogID = repository.GetLastDog().ID,
                CategoryID = repository.GetCategoryByName(CategorySelected).ID,
                Description = model.MemberData.Description
            });
            repository.Save();
            return RedirectToAction("Index");
        }

        #endregion

        #region Temp Data

        //Temp file data for preview image before loading on the server
        static byte[] imageData = null;

        //Calculating pagination for "Members" page.
        public void CalculatePagination(ServerDbRepository repository,int pageNamber)
        {
            ViewBag.ListFrom = pageNamber * 9;
            ViewBag.ListTo = ViewBag.ListFrom + 9;
            ViewBag.PagesCount = (int)(repository.GetMembers().ToList().Count / 9);
            if ((int)(repository.GetMembers().ToList().Count % 9) > 0) ViewBag.PagesCount++;
            ViewBag.ActivePage = pageNamber;
        }
        #endregion
    }
}