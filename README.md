# Husky Competition #

This is a project that will allow you and your pupils to take part in the competition 
for the title of Mr. or Mrs. season.
Currently open categories such as the 
"Winter Season", "Summer Season", "Autumn Season", "Spring Season".
In all an equal chance. 
Send us photos of your Husky and we'll see who's cooler!


### About Version ###

* ASP.NET MVC Application 
* First initial release
* Version 1.0.0

### Install ###

* Choose hosting provider and download application
* Configuration for the project contains in Web.config file
* Database avalible in App_Data catalog
* Database connection string contains in Web.config file (named DeffaultConnection)
* Copy all the files and put into root catalog on the hosting, 
  configured for ASP.NET MVC 4 applications.

### Maxim Tishenko ###
* Skype maximcha11
* Email maxim.tis96@gmail.com
